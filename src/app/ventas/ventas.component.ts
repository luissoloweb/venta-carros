import { Component, OnInit } from '@angular/core';
import { BaseService } from './../services/base.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {

  public ventas = [];
  public clic = false;
  public ventaIndex;
  public ventaSelected;

  editVentaA = {
    cliente: {
      nombre: ''
    },
    empleado: {
      nombre: ''
    },
    carro: {
      nombre: '',
      precio: ''
    },
    comision_cliente: '',
    enganche: '',
    plazo: '',
    costo_total: ''
  };
  tabla;

  constructor(private baseService: BaseService<any>,
              private location: Location
              ) { }

  ngOnInit() {
    this.baseService.get('http://localhost:3001/api/ventas').subscribe(ventas => {
      console.log(ventas.entity);
      this.ventas = ventas.entity;
    });
  }

  goBack(): void {
    this.location.back();
  }

  deleteVenta(index) {
    const id = this.ventas[index]._id;
    this.baseService.delete('http://localhost:3001/api/venta/' + id).subscribe(res => {
      this.ventas.splice(index, 1);
    }, err => {
      console.log(err);
    });
  }

  mostrarVenta(index) {
    this.ventaIndex = index;
    this.ventaSelected = this.ventas[index];
    this.editVentaA = this.ventaSelected;
    this.clic = !this.clic;
    console.log(this.ventaSelected);
  }

  editVenta() {
    const id = this.ventas[this.ventaIndex]._id;
    this.baseService.put('http://localhost:3001/api/venta/' + id, this.editVentaA).subscribe(res => {
      this.ventas[this.ventaIndex] = res.entity;
    }, err => {
      console.log(err);
    });
  }

  changeClic() {
    this.clic = !this.clic;
  }

 /* crearTabla(numFilas, dateInput, mensualidad, saldo) {
    console.log(numFilas, dateInput, mensualidad, saldo);
    let tabla = [];
    let date = dateInput; // 19/5/2020

    for (let i = 0; i < numFilas; i++) {
      tabla.push({
        num: i + 1,
        fecha: date,
        concepto: 'Abono de Mensualidad',
        abono: parseInt(mensualidad),
        saldo:
          parseInt(saldo) -
          parseInt(mensualidad) * (i + 1)
      });

      tabla[i].interes = (tabla[i].saldo * 0.12) / 12;
      tabla[i].mensualidad =
      tabla[i].interes + parseInt(mensualidad);
    }

    let fila = `<tr>
        <th>No.</th>
        <th>Fecha</th>
        <th>Concepto</th>
        <th>Abono</th>
        <th>Interés</th>
        <th>Mensualidad</th>
        <th>Saldo</th>
    </tr>
    `;

    for (let i = 0; i < tabla.length; i++) {
      fila += `
        <tr>
            <td>${tabla[i].num}</td>
            <td>${tabla[i].fecha}</td>
            <td>${tabla[i].concepto}</td>
            <td>${tabla[i].abono}</td>
            <td>${tabla[i].interes}</td>
            <td>${tabla[i].mensualidad}</td>
            <td>${tabla[i].saldo}</td>
        </tr>
        `;

      this.tabla.innerHTML = fila;
    }
  }

  cerrarTabla() {
    this.tabla.innerHTML = '';
  }
*/
}
