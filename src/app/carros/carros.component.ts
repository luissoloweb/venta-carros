import { BaseService } from './../services/base.service';
import { UploadImagesService } from './../services/upload-images.service';
import { Component, OnInit } from '@angular/core';
import { findIndex } from 'rxjs/operators';

class ImageSnippet {
  constructor(public src: string, public file: File) { }
}

@Component({
  selector: 'app-carros',
  templateUrl: './carros.component.html',
  styleUrls: ['./carros.component.css']
})
export class CarrosComponent implements OnInit {
  modal = {
    edit: {
      visible: false
    },
    new: {
      visible: false
    }
  };
  newCar = {
    nombre: '',
    modelo: '',
    marca: '',
    color: '',
    precio: null,
    imagen: ''
  };
  editCar = {
    nombre: '',
    modelo: '',
    marca: '',
    color: '',
    precio: null,
    imagen: '',
    _id: ''
  };
  private carSelectedUpdate;
  public btnInputLoadImage;
  public btnInputLoadImageEdit;
  public selectedFile: ImageSnippet;
  public carros = [];
  constructor(private imageService: UploadImagesService, private baseService: BaseService<any>) { }

  ngOnInit() {
    this.btnInputLoadImage = document.getElementById('img-product');
    this.btnInputLoadImageEdit = document.getElementById('img-product-edit');
    this.baseService.get('http://localhost:3001/api/carros').subscribe(res => {
      this.carros = res.entity;
      console.log(this.carros)
    }, err => {
      console.log(err);
    });
  }

  public openChoiceImage(typeButton) {
    if (typeButton === 'newProduct') {
      this.btnInputLoadImage.click();
    } else if (typeButton === 'editProduct') {
      this.btnInputLoadImageEdit.click();
    }
  }

  public uploadImage(imageInput, typeButton) {
    const file = imageInput.srcElement.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.selectedFile = new ImageSnippet(event.target.result, file);
      this.imageService.postImage(file).subscribe(res => {
        if (typeButton === 'newProduct') {
          alert('nuevo')
          this.newCar.imagen = 'http://localhost:3001/images/' + res[0].filename;
          document.getElementById('img-product')['value'] = '';
        } else if (typeButton === 'editProduct') {
          alert('editar')
          this.editCar.imagen = 'http://localhost:3001/images/' + res[0].filename;
        }
      }, err => {
        console.log(err);
      });
    });

    reader.readAsDataURL(file);
  }

  public createCar() {
    this.baseService.post('http://localhost:3001/api/carro', this.newCar).subscribe(res => {
      console.log(res.entity);
      this.carros.push(res.entity);
      this.modal.new.visible = false;
    }, err => {
      console.log(err);
    });
  }

  openModalNewCar() {
    this.modal.new.visible = true;
  }

  closeModalNewCar() {
    this.modal.new.visible = false;
  }

  openModalEditCar(index) {
    this.carSelectedUpdate = this.carros[index];
    this.editCar = this.carSelectedUpdate;
    this.modal.edit.visible = true;
  }

  deleteCar(index) {
    const id = this.carros[index]._id;
    this.baseService.delete('http://localhost:3001/api/carro/' + id).subscribe(res => {
      this.carros.splice(index, 1);
    }, err => {
      console.log(err);
    });
  }

  updateCar() {
    console.log(this.carSelectedUpdate);
    this.baseService.put('http://localhost:3001/api/carro/' + this.carSelectedUpdate['_id'], this.editCar).subscribe(res => {
      const indexCar = this.carros.findIndex(car => car._id === this.carSelectedUpdate['_id']);
      console.log(indexCar);
      this.carros[indexCar] = res.entity;
      this.modal.edit.visible = false;
    }, err => {
      console.log(err);
    });
  }

  closeModalEditCar() {
    this.modal.edit.visible = false;
  }
}
