import { appRoutes } from './app.routes';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from "@angular/common/http";
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ClientesComponent } from './clientes/clientes.component';
import { VendedoresComponent } from './vendedores/vendedores.component';
import { VentaComponent } from './venta/venta.component';
import { CotizacionComponent } from './cotizacion/cotizacion.component';
import { CarrosComponent } from './carros/carros.component';
import { FormsModule } from '@angular/forms';
import { CotizationsComponent } from './cotizations/cotizations.component';
import { BaseService } from './services/base.service';
import { VentasComponent } from './ventas/ventas.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ClientesComponent,
    VendedoresComponent,
    VentaComponent,
    CotizacionComponent,
    CarrosComponent,
    CotizationsComponent,
    VentasComponent
  ],
  imports: [
    BrowserModule,
    appRoutes,
    HttpClientModule,
    FormsModule
  ],
  providers: [BaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
