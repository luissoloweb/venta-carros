import { CotizationsComponent } from './cotizations/cotizations.component';
import { VentaComponent } from './venta/venta.component';
import { CotizacionComponent } from './cotizacion/cotizacion.component';
import { ClientesComponent } from './clientes/clientes.component';
import { VendedoresComponent } from './vendedores/vendedores.component';
import { CarrosComponent } from './carros/carros.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { VentasComponent } from './ventas/ventas.component';

const routes = [
    {path: 'header', component: HeaderComponent},
    {path: 'venta', component: VentaComponent},
    {path: 'cotizacion', component: CotizacionComponent},
    {path: 'cotizaciones', component: CotizationsComponent},
    {path: 'clientes', component: ClientesComponent},
    {path: 'vendedores', component: VendedoresComponent},
    {path: 'carros', component: CarrosComponent},
    {path: 'ventas', component: VentasComponent}
];

export const appRoutes = RouterModule.forRoot(routes);
