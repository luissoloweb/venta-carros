import { BaseService } from './base.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UploadImagesService {
  private url = 'http://localhost:3001/api/';
  constructor(private http: HttpClient) { }

  public postImage(image):Observable<any> {
     const formData = new FormData();
     formData.append('image', image);
     let headers = new HttpHeaders().set('Content-Type', 'multipart/form-data');
     return this.http.post(this.url + 'upload-image', formData);
  }
}