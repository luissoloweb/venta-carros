import { Component, OnInit, Input} from '@angular/core';
import { BaseService } from './../services/base.service';

@Component({
  selector: 'app-cotizations',
  templateUrl: './cotizations.component.html',
  styleUrls: ['./cotizations.component.css']
})
export class CotizationsComponent implements OnInit {

  public cotizaciones = [];
  private cotSelectedUpdate;
  private cotizacionIndex;

  modal = {
    edit: {
      visible: false
    }
  };

  editCotizacion = {
    cliente: {
      nombre: ''
    },
    carro: {
      nombre: '',
      modelo: ''
    },
    importe: '',
    enganche: '',
    importe_enganche: '',
    tasa: '',
    plazo: '',
    saldo: ''
  };
  tabla;
  constructor(
    private baseService: BaseService<any>,
    ) { }

  ngOnInit() {
    this.baseService.get('http://localhost:3001/api/cotizaciones').subscribe(cotizaciones => {
      console.log(cotizaciones.entity);
      this.cotizaciones = cotizaciones.entity;
    });
    this.tabla = document.getElementById('tabla');
  }

  openModalEditCotizacion(index) {
    this.cotizacionIndex = index;
    this.cotSelectedUpdate = this.cotizaciones[index];
    this.editCotizacion = this.cotSelectedUpdate;
    this.modal.edit.visible = true;
    console.log(this.cotSelectedUpdate);
  }

  updateCotizacion() {
    const id = this.cotizaciones[this.cotizacionIndex]._id;
    this.baseService.put('http://localhost:3001/api/cotizacion/' + id, this.editCotizacion).subscribe(res => {
      this.cotizaciones[this.cotizacionIndex] = res.entity;
      this.modal.edit.visible = false;
    }, err => {
      console.log(err);
    });
  }

  deleteCotizacion(index) {
    const id = this.cotizaciones[index]._id;
    this.baseService.delete('http://localhost:3001/api/cotizacion/' + id).subscribe(res => {
      this.cotizaciones.splice(index, 1);
    }, err => {
      console.log(err);
    });
  }

  closeModalEditCot() {
    this.modal.edit.visible = false;
  }

  crearTabla(numFilas, dateInput, mensualidad, saldo) {
    console.log(numFilas, dateInput, mensualidad, saldo);
    let tabla = [];
    let date = dateInput; // 19/5/2020

    for (let i = 0; i < numFilas; i++) {
      tabla.push({
        num: i + 1,
        fecha: date,
        concepto: 'Abono de Mensualidad',
        abono: parseInt(mensualidad),
        saldo:
          parseInt(saldo) -
          parseInt(mensualidad) * (i + 1)
      });

      tabla[i].interes = (tabla[i].saldo * 0.12) / 12;
      tabla[i].mensualidad =
      tabla[i].interes + parseInt(mensualidad);
    }

    let fila = `<tr>
        <th>No.</th>
        <th>Fecha</th>
        <th>Concepto</th>
        <th>Abono</th>
        <th>Interés</th>
        <th>Mensualidad</th>
        <th>Saldo</th>
    </tr>
    `;

    for (let i = 0; i < tabla.length; i++) {
      fila += `
        <tr>
            <td>${tabla[i].num}</td>
            <td>${tabla[i].fecha}</td>
            <td>${tabla[i].concepto}</td>
            <td>${tabla[i].abono}</td>
            <td>${tabla[i].interes}</td>
            <td>${tabla[i].mensualidad}</td>
            <td>${tabla[i].saldo}</td>
        </tr>
        `;

      this.tabla.innerHTML = fila;
    }
  }

  cerrarTabla() {
    this.tabla.innerHTML = '';
  }
}
