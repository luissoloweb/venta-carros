import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizationsComponent } from './cotizations.component';

describe('CotizationsComponent', () => {
  let component: CotizationsComponent;
  let fixture: ComponentFixture<CotizationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotizationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
