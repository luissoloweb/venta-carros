import { BaseService } from './../services/base.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-vendedores',
  templateUrl: './vendedores.component.html',
  styleUrls: ['./vendedores.component.css']
})
export class VendedoresComponent implements OnInit {
  modal = {
    edit: {
      visible: false
    },
    new: {
      visible: false
    }
  };
  public newClient = {
    nombre: '',
    direccion: '',
    correo: '',
    telefono: '',
    sueldo: null
  };
  public editClient = {
    nombre: '',
    direccion: '',
    correo: '',
    telefono: '',
    sueldo: null
  };
  private editClientIndex;
  public nombre = '';
  public clients = [];
  @ViewChild('input1') input1;
  constructor(private baseService: BaseService<any>) { }

  ngOnInit() {
    this.baseService.get('http://localhost:3001/api/vendedores').subscribe(res => {
      this.clients = res.entity;
      console.log(res.entity);
    }, err => {
      console.log(err);
    });
  }

  openModalNewClient() {
    this.modal.new.visible = true;
  }

  closeModalNewClient() {
    this.modal.new.visible = false;
  }

  openModalEditClient(index) {
    this.editClientIndex = index;
    this.editClient = this.clients[index];
    this.modal.edit.visible = true;
  }

  closeModalEditClient() {
    this.modal.edit.visible = false;
  }

  deleteClient(index) {
    const id = this.clients[index]._id;
    this.baseService.delete('http://localhost:3001/api/vendedor/' + id).subscribe(res => {
      this.clients.splice(index, 1);
    }, err => {
      console.log(err);
    });
  }

  updateClient() {
    const id = this.clients[this.editClientIndex]._id;
    this.baseService.put('http://localhost:3001/api/vendedor/' + id, this.editClient).subscribe(res => {
      this.clients[this.editClientIndex] = res.entity;
      this.modal.edit.visible = false;
    }, err => {
      console.log(err);
    });
  }

  createClient() {
    console.log(this.newClient);
    this.baseService.post('http://localhost:3001/api/vendedor', this.newClient).subscribe(res => {
      console.log(res);
      this.clients.push(res.entity);
      this.modal.new.visible = false;
    }, err => {
      console.log(err);
    });
  }
}
