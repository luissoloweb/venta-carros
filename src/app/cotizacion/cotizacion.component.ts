import { BaseService } from './../services/base.service';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-cotizacion',
  templateUrl: './cotizacion.component.html',
  styleUrls: ['./cotizacion.component.css']
})
export class CotizacionComponent implements OnInit {
  public selectAuto;
  public selectModel;
  public enganche;
  public importe;
  public importeEnganche;
  public saldo;
  public mensualidad;
  public plazo;
  public tasa;
  public botonCalcular;
  public tablap;
  public tabla = [];
  public selectClientes;
  public autos = [
    {
      name: 'Jetta',
      charac: [
        { model: '2015', price: 200000 },
        { model: '2016', price: 220000 },
        { model: '2017', price: 240000 }
      ]
    },
    {
      name: 'Mini Cooper',
      charac: [
        { model: '2015', price: 300000 },
        { model: '2016', price: 320000 },
        { model: '2017', price: 340000 }
      ]
    },
    {
      name: 'Porsche',
      charac: [
        { model: '2017', price: 600000 },
        { model: '2018', price: 620000 },
        { model: '2019', price: 640000 }
      ]
    },
    {
      name: 'Hummer',
      charac: [
        { model: '2017', price: 800000 },
        { model: '2018', price: 820000 },
        { model: '2019', price: 840000 }
      ]
    }
  ];
  public clientSelected;
  public carSelected;
  public modelos = [
    { model: '2015', price: 200000 },
    { model: '2016', price: 220000 },
    { model: '2017', price: 240000 }
  ];
  public date_cozitation;
  public clientes = [];

  public cotizaciones = [];

  constructor(private baseService: BaseService<any>) {}

  ngOnInit() {
    this.selectAuto = document.getElementById('autos');
    this.selectModel = document.getElementById('modelo');
    this.enganche = document.getElementById('enganche');
    this.importe = document.getElementById('importe');
    this.importeEnganche = document.getElementById('importe_enganche');
    this.saldo = document.getElementById('saldo');
    this.mensualidad = document.getElementById('mensualidad');
    this.plazo = document.getElementById('plazo');
    this.tasa = document.getElementById('tasa');
    this.botonCalcular = document.getElementById('calcular');
    this.tablap = document.getElementById('tabla');
    this.selectClientes = document.getElementById('clientes');
    this.baseService.get('http://localhost:3001/api/clientes').subscribe(
      res => {
        this.clientes = res.entity;
        console.log(this.clientes);
        this.baseService.get('http://localhost:3001/api/carros').subscribe(carros => {
          this.autos = carros.entity;
          console.log(this.autos);
          this.cargar();
        }, err => {
          console.log(err);
        });
      },
      err => {
        console.log(err);
      }
    );
  }

  keyupEnganche(event) {
    this.importeEnganche.value =
      (parseInt(this.enganche.value, 10) * parseInt(this.importe.value, 10)) /
      100;
    this.saldo.value =
      parseInt(this.importe.value) -
      parseInt(this.importeEnganche.value) +
      parseInt(this.importe.value) * 0.12;
  }

  keyupPlazo(event) {
    this.mensualidad.value =
      parseInt(this.saldo.value) / parseInt(this.plazo.value);
  }

  public calcular2() {
    if (parseInt(this.plazo.value, 10) > 0) {
      this.crearTabla(parseInt(this.plazo.value));
    } else {
      alert('Escriba el plazo');
    }
  }

  cargar() {
    for (let i = 0; i < this.clientes.length; i++) {
      const option3 = document.createElement('option');
      option3.innerHTML = this.clientes[i].nombre;
      this.selectClientes.appendChild(option3);
    }

    for (let i = 0; i < this.autos.length; i++) {
      const option = document.createElement('option');
      option.innerHTML = this.autos[i]['nombre'];
      this.selectAuto.appendChild(option);
    }

    for (let i = 0; i < 1; i++) {
      const option2 = document.createElement('option');
      option2.innerHTML = this.autos[i]['modelo'];
      this.selectModel.appendChild(option2);
    }
  }

  selectClient(event) {
    this.clientSelected = this.clientes[event.target.selectedIndex - 1];
    console.log(this.clientSelected);
  }

  changeAutos(event) {
    this.selectModel.innerHTML = '';
    console.log(this.autos);
    const autoSelect = this.autos[this.selectAuto.selectedIndex - 1];
    console.log(autoSelect);
    this.carSelected = autoSelect;
    for (let i = 0; i < 1; i++) {
      const option2 = document.createElement('option');
      option2.innerHTML = autoSelect['modelo'];
      this.selectModel.appendChild(option2);
    }

    this.importe.value = autoSelect['precio'];

    /*this.selectModel.addEventListener('change', e => {
      const modeloSelect = this.selectModel.options[
        this.selectModel.selectedIndex
      ].value;
      for (let i = 0; i < autoSelect.charac.length; i++) {
        if (autoSelect.charac[i].model === modeloSelect) {
          this.importe.value = autoSelect.charac[i].price;
        }
      }
    });*/
  }

  crearTabla(numFilas) {
    this.tabla = [];
    const date = document.querySelector('#fecha');

    for (let i = 0; i < numFilas; i++) {
      const partesFecha = date['value'].split('-');
      const d = new Date(
        partesFecha[0] +
          '-' +
          partesFecha[1] +
          '-' +
          (parseInt(partesFecha[2], 10) + 1)
      );
      d.setMonth(d.getMonth() + (i + 1));
      const fecha1 =
        d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
        this.date_cozitation = fecha1;
      this.tabla.push({
        num: i + 1,
        fecha: fecha1,
        concepto: 'Abono de Mensualidad',
        abono: parseInt(this.mensualidad.value),
        saldo:
          parseInt(this.saldo.value) -
          parseInt(this.mensualidad.value) * (i + 1)
      });

      this.tabla[i].interes = (this.tabla[i].saldo * 0.12) / 12;
      this.tabla[i].mensualidad =
        this.tabla[i].interes + parseInt(this.mensualidad.value);
    }

    let fila = `<tr>
        <th>No.</th>
        <th>Fecha</th>
        <th>Concepto</th>
        <th>Abono</th>
        <th>Interés</th>
        <th>Mensualidad</th>
        <th>Saldo</th>
    </tr>
    `;

    for (let i = 0; i < this.tabla.length; i++) {
      fila += `
        <tr>
            <td>${this.tabla[i].num}</td>
            <td>${this.tabla[i].fecha}</td>
            <td>${this.tabla[i].concepto}</td>
            <td>${this.tabla[i].abono}</td>
            <td>${this.tabla[i].interes}</td>
            <td>${this.tabla[i].mensualidad}</td>
            <td>${this.tabla[i].saldo}</td>
        </tr>
        `;

      this.tablap.innerHTML = fila;
    }
  }

  saveCotization() {
     const cotization = {
      cliente: {
        nombre: this.clientSelected.nombre ,
        id: this.clientSelected._id
      },
      carro: {
        nombre: this.carSelected.nombre,
        modelo: this.carSelected.modelo,
        color: this.carSelected.color,
        marca: this.carSelected.marca,
        precio:  this.carSelected.precio,
        imagen: this.carSelected.imagen
      },
      importe: parseInt(this.importe.value, 10),
      importe_enganche: parseInt(this.importeEnganche.value, 10),
      tasa: parseInt(this.tasa.value, 10),
      saldo: parseInt(this.saldo.value, 10),
      enganche: parseInt(this.enganche.value, 10),
      mensualidad: parseInt(this.mensualidad.value, 10),
      fecha_cotizacion: this.date_cozitation,
      plazo: parseInt(this.plazo.value, 10),
    };
     this.baseService.post('http://localhost:3001/api/cotizacion', cotization).subscribe(res => {
      this.cotizaciones.push(res.entity);
      console.log(res.entity);
    }, err => {
      console.log(err);
    });
  }
}
