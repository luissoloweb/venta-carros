import { Component, OnInit } from '@angular/core';
import { BaseService } from './../services/base.service';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css']
})
export class VentaComponent implements OnInit {
  public selectAuto;
  public selectModel;
  public enganche;
  public importe;
  public importeEnganche;
  public precioNeto;
  public mensualidad;
  public tablap;
  public tabla = [];
  public selectClientes;
  public descuento;
  public plazo;
  public selectVendedor;
  public comision;

  public ventas = [];
  public autos = [];
  public clientes = [];
  public vendedores = [];

  public vendedorSelected;
  public clientSelected;
  public carSelected;

  constructor(private baseService: BaseService<any>) { }

  ngOnInit() {
    this.selectAuto = document.getElementById('autos');
    this.enganche = document.getElementById('enganche');
    this.importe = document.getElementById('importe');
    this.importeEnganche = document.getElementById('importe_enganche');
    this.precioNeto = document.getElementById('precioNeto');
    this.mensualidad = document.getElementById('mensualidad');
    this.tablap = document.getElementById('tabla');
    this.selectClientes = document.getElementById('clientes');
    this.descuento = document.getElementById('descuento');
    this.plazo = document.getElementById('plazo');
    this.selectVendedor = document.getElementById('vendedores');
    this.comision = document.getElementById('comision');

    this.baseService.get('http://localhost:3001/api/clientes').subscribe(
      res => {
        this.clientes = res.entity;
        console.log(this.clientes);
        this.baseService.get('http://localhost:3001/api/carros').subscribe(carros => {
          this.autos = carros.entity;
          console.log(this.autos);
          this.cargar();
        }, err => {
          console.log(err);
        });
      },
      err => {
        console.log(err);
      }
    );

    this.baseService.get('http://localhost:3001/api/vendedores').subscribe(res => {
      this.vendedores = res.entity;
      console.log(res.entity);
    }, err => {
      console.log(err);
    });
  }

  cargar() {
    for (let i = 0; i < this.clientes.length; i++) {
      const option3 = document.createElement('option');
      option3.innerHTML = this.clientes[i].nombre;
      this.selectClientes.appendChild(option3);
    }

    for (let i = 0; i < this.autos.length; i++) {
      const option = document.createElement('option');
      option.innerHTML = this.autos[i]['nombre'];
      this.selectAuto.appendChild(option);
    }

    for (let i = 0; i < this.vendedores.length; i++) {
      const option2 = document.createElement('option');
      option2.innerHTML = this.vendedores[i].nombre;
      this.selectVendedor.appendChild(option2);
    }
  }

  selectionVendedor(event) {
    this.vendedorSelected = this.vendedores[event.target.selectedIndex - 1];
    console.log(this.vendedorSelected);
  }

  selectClient(event) {
    this.clientSelected = this.clientes[event.target.selectedIndex - 1];
    console.log(this.clientSelected);
    console.log(event.target.selectedIndex);
  }

  selectCarro(event) {
    this.carSelected = this.autos[event.target.selectedIndex - 1];
    console.log(this.carSelected);
    const autoSelect = this.autos[this.selectAuto.selectedIndex - 1];
    this.importe.value = autoSelect['precio'];
  }

  keyupDescuento(event) {
    this.precioNeto.value = (parseInt(this.importe.value, 10) - (parseInt(this.importe.value, 10) * parseInt(this.descuento.value, 10) / 100)) - parseInt(this.enganche.value);
  }

  guardarVenta() {
    console.log(this.vendedorSelected);
    console.log(this.clientSelected);
    console.log(this.carSelected);
    const venta = {
      empleado: {
        nombre: this.vendedorSelected['nombre'],
        id: this.vendedorSelected._id
      },
      cliente: {
        nombre: this.clientSelected['nombre'],
        id: this.clientSelected._id
      },
      carro: {
        nombre: this.carSelected.nombre,
        modelo: this.carSelected.modelo,
        color: this.carSelected.color,
        marca: this.carSelected.marca,
        precio:  this.carSelected.precio,
        imagen: this.carSelected.imagen
      },
      comision_cliente: parseInt(this.comision.value, 10) * parseInt(this.precioNeto.value, 10) / 100,
      enganche: parseInt(this.enganche.value, 10),
      plazo: parseInt(this.plazo, 10),
      costo_total: parseInt(this.precioNeto.value, 10),
    };

    this.baseService.post('http://localhost:3001/api/venta', venta).subscribe(res => {
      this.ventas.push(res.entity);
      console.log(res.entity);
    }, err => {
      console.log(err);
    });

  }

}
