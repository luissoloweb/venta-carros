import { BaseService } from './../services/base.service';
import { Component, OnInit, ViewChild } from '@angular/core';


@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  modal = {
    edit: {
      visible: false
    },
    new: {
      visible: false
    }
  };
  public newClient = {
    nombre: '',
    direccion: '',
    correo: '',
    telefono: '',
    saldo: null
  };
  public editClient = {
    nombre: '',
    direccion: '',
    correo: '',
    telefono: '',
    saldo: null
  };
  private editClientIndex;
  public nombre = '';
  public clients = [];
  @ViewChild('input1') input1;
  constructor(private baseService: BaseService<any>) { }

  ngOnInit() {
    this.baseService.get('http://localhost:3001/api/clientes').subscribe(res => {
      this.clients = res.entity;
      console.log(res.entity);
    }, err => {
      console.log(err);
    });
  }

  openModalNewClient() {
    this.modal.new.visible = true;
  }

  closeModalNewClient() {
    this.modal.new.visible = false;
  }

  openModalEditClient(index) {
    this.editClientIndex = index;
    this.editClient = this.clients[index];
    this.modal.edit.visible = true;
  }

  closeModalEditClient() {
    this.modal.edit.visible = false;
  }

  deleteClient(index) {
    const id = this.clients[index]._id;
    this.baseService.delete('http://localhost:3001/api/cliente/' + id).subscribe(res => {
      this.clients.splice(index, 1);
    }, err => {
      console.log(err);
    });
  }

  updateClient() {
    const id = this.clients[this.editClientIndex]._id;
    this.baseService.put('http://localhost:3001/api/cliente/' + id, this.editClient).subscribe(res => {
      this.clients[this.editClientIndex] = res.entity;
      this.modal.edit.visible = false;
    }, err => {
      console.log(err);
    });
  }

  createClient() {
    console.log(this.newClient);
    this.baseService.post('http://localhost:3001/api/cliente', this.newClient).subscribe(res => {
      console.log(res);
      this.clients.push(res.entity);
      this.modal.new.visible = false;
      this.newClient.nombre = "";
      this.newClient.direccion = "";
      this.newClient.correo = "";
      this.newClient.telefono = "";
      this.newClient.saldo = "";
    }, err => {
      console.log(err);
    });
  }

}
